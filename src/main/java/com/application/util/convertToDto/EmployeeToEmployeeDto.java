package com.application.util.convertToDto;

import com.application.dto.EmployeeDto;
import com.application.entity.Employee;

public class EmployeeToEmployeeDto {
	
	public static EmployeeDto convertToEmployeeDto(Employee entity){
		EmployeeDto dto = new EmployeeDto();
		dto.setId(entity.getId());
		dto.setFirstName(entity.getFirstName());
		dto.setLastName(entity.getLastName());
		dto.setAge(entity.getAge());
		dto.setAddress(entity.getAddress());
		return dto;
	}

}
