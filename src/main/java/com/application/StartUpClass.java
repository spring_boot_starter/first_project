package com.application;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.SpringBootConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.ImportResource;

@SpringBootConfiguration
@ImportResource("classpath:security.xml")
@ComponentScan({"com.application.repository","com.application.service","com.application.dao","com.application.controller"})
 class StartUpClass {
	
	public static void main(String[] args){
		SpringApplication.run(StartUpClass.class, args);
	}

}
