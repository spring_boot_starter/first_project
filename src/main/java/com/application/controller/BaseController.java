package com.application.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.application.dto.EmployeeDto;
import com.application.service.EmployeeService;



@RestController

public class BaseController {
	private static final org.apache.log4j.Logger log = org.apache.log4j.LogManager.getLogger(BaseController.class.getName());
	
	@Autowired
	EmployeeService employeeService;
	
	
	@RequestMapping(value="/getInfo",method=RequestMethod.GET)
	public String getRequest(HttpServletRequest reuest,HttpServletResponse response){
		log.info("Controller called");
		return "Request Received";
	}
	
	
	@RequestMapping(value="/create/employee",method=RequestMethod.POST)
	public EmployeeDto createEmployee(@RequestBody EmployeeDto dto,HttpServletRequest reuest,HttpServletResponse response){
		log.info("<<=========create employee controller=============>>");
		
		return employeeService.createEmployee(dto);
	}
	
	
}
