package com.application.util.convertToEntity;

import com.application.dto.EmployeeDto;
import com.application.entity.Employee;

public class EmployeeDtoToEmployee {
	
	public static Employee convertToEmployee(EmployeeDto dto){
		Employee entity = new Employee();
		entity.setFirstName(dto.getFirstName());
		entity.setLastName(dto.getLastName());
		entity.setAge(dto.getAge());
		entity.setAddress(dto.getAddress());
		return entity;
	}

}
