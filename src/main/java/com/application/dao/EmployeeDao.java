package com.application.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.application.entity.Employee;
import com.application.repository.EmployeeRepository;

@Service("employeeDao")
public class EmployeeDao {
	private static final org.apache.log4j.Logger log = org.apache.log4j.LogManager.getLogger(EmployeeDao.class.getName());
	@Autowired
	EmployeeRepository employeeRepository;

	public Employee createEmployee(Employee entity){
		log.info("<<=============createEmployee in DAO ============>>");
		if(employeeRepository==null)
			log.info("<<============= employeeRepository NULL =============>>");
		if(entity!=null){
			entity=employeeRepository.save(entity);
		}
		return entity;
	}
	
}
