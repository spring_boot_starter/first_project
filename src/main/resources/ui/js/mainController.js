(function() {
	
	var app = angular.module("RAMAPURAM", [ 'ngRoute', 'ngResource','ui.bootstrap', 'angular-loading-bar', 'textAngular','tree.directives', 'ngCookies', 'ngMaterial','ngStorage','oc.lazyLoad','ngTable']);
	
	app.config(function($routeProvider,cfpLoadingBarProvider,$httpProvider) {
		
		 $httpProvider.interceptors.push('httpInterceptor');
		 
		 $routeProvider
		 
		 .when('/', {
				controller : "LoginController",
				templateUrl : 'ui/index.html',
				data: {
					requireLogin: false,
					feature: 'ALLOW_ALL'
				},
				resolve: {
	                lazy: ['$ocLazyLoad', function($ocLazyLoad) {
	                    return $ocLazyLoad.load([{
	                        name: 'PATApp',
	                        files: ['html/login/login-controller.js']
	                    }]);
	                }]
	            }	
				
			})
		 
		 .when("/anoother page",{
			 controller : "homepageController",
			 templateUrl :'/ui/index.html',
				data: {
					feature: 'ALLOW_ALL'
				},
				resolve: {
	                lazy: ['$ocLazyLoad', function($ocLazyLoad) {
	                    return $ocLazyLoad.load([{
	                        name: 'PATApp',
	                        files: ['/ui/js/homeController.js']
	                    }]);
	                }]
	            }
		 })
	});
	
	angular.module('RAMAPURAM').factory('StoreService', function () {
        var storedObject;
        return {
            set: function (o) {
                this.storedObject = o;
            },
            get: function () {
                return this.storedObject;
            }
        };
});
	
	app.run(function($rootScope, $location, $cookieStore, CurrentUserAPI,Resource, $window,$localStorage,$cookies) {
		
		$rootScope.$on('$routeChangeError',function(event, next, current){
			$location.path("/");
		});
	});
	
	app.factory("PermissionRequest", function($resource) {
		return $resource("/permissionRequest/:id", {}, {
			get : {method : 'GET',params : {},isArray : true},
			query : {method : 'GET',params : {id : ''},isArray : false},
			save : {method : 'POST',isArray : false}
		});
	});
	
	
	app.filter('offset', function() {
		return function(input, start) {
			start = parseInt(start, 10);
			if (angular.isArray(input) && input.length > 0)
				return input.slice(start);
			return input;
		};
	});

});