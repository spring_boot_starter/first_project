package com.application.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.application.dao.EmployeeDao;
import com.application.dto.EmployeeDto;
import com.application.entity.Employee;
import com.application.util.convertToDto.EmployeeToEmployeeDto;
import com.application.util.convertToEntity.EmployeeDtoToEmployee;

@Service("emloyeeService")
public class EmployeeService {
	private static final org.apache.log4j.Logger log = org.apache.log4j.LogManager.getLogger(EmployeeService.class.getName());
	
	
//	private EmployeeDao employeeDao = new EmployeeDao(); 	EmployeeDao
	@Autowired
	EmployeeDao employeeDao;

	
	public EmployeeDto  createEmployee(EmployeeDto dto){
		if(employeeDao==null)
			log.info("<<==========employeeDao null in emloyeeService==============>>");
		Employee entity = EmployeeDtoToEmployee.convertToEmployee(dto);
		if(employeeDao.createEmployee(entity)!= null && employeeDao.createEmployee(entity).getId() != null)
			log.info("<<==========EEMPLOYEE CREATED==========>>");
		dto = EmployeeToEmployeeDto.convertToEmployeeDto(entity);
		return dto;
	}
	
}
